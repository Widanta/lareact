const isPosts = ({ posts }) => {
    return posts.map((posts, i) => {
        return (
            <div key={i} className="card w-full lg:w-96 bg-base-100 shadow-xl">
                <figure>
                    <img src="https://placeimg.com/400/225/arch" alt="Shoes" />
                </figure>
                <div className="card-body">
                    <h2 className="card-title">
                        {posts.title}
                        <div className="badge badge-secondary">NEW</div>
                    </h2>
                    <p>{posts.slug}</p>
                    <p>{posts.content}</p>
                    <div className="card-actions justify-end">
                        <div className="badge badge-outline">
                            {posts.category.name}
                        </div>
                    </div>
                </div>
            </div>
        );
    });
};

const noPosts = () => {
    return <div>Saat ini belum ada berita</div>;
};

const PostsCards = ({ posts }) => {
    if (!posts) {
        return noPosts();
    } else {
        return isPosts({ posts });
    }
};

export default PostsCards;
