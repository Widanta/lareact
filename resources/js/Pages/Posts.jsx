import React from "react";
import { Head } from "@inertiajs/inertia-react";
import Navbar from "@/Components/Navbar";
import PostsCards from "@/Components/Posts/PostsCard";
import Paginator from "@/Components/Posts/Paginator";

export default function Posts(props) {
    return (
        <div className="min-h-screen bg-slate-50">
            <Head title={props.title} />
            <Navbar user={props.auth.user}/>
            <div className="flex justify-center flex-col lg:flex-row lg:flex-wrap lg:items-stretch xl:flex-row xl:flex-wrap xl:items-stretch items-center gap-4 py-4">
                <PostsCards posts={props.posts.data} />
            </div>
            <div className="flex justify-center items-center">
                <Paginator meta={props.posts.meta} />
            </div>
        </div>
    );
}
