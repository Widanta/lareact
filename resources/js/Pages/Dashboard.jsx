import React, { useState } from 'react';
import Authenticated from '@/Layouts/Authenticated';
import { Head } from '@inertiajs/inertia-react';
import { Inertia } from '@inertiajs/inertia';

export default function Dashboard(props) {
    const [title, setTitle] = useState('');
    const [slug, setSlug] = useState('');
    const [content, setContent] = useState('');
    const [category_id, setCategory] = useState('');
    const [isNotif, setNotif] = useState(false);

    const handleSubmit = () => {
        const data = {
            title, slug, content, category_id 
        }
        Inertia.post('/posts/store', data);
        setNotif(true);

        // reset input values
        setTitle('')
        setSlug('')
        setContent('')
        setCategory('')
    }

    console.log('last props: ', props)

    return (
        <Authenticated
            auth={props.auth}
            errors={props.errors}
            header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">Dashboard</h2>}
        >
            <Head title="Dashboard" />

            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className="p-3 bg-white overflow-hidden shadow-sm sm:rounded-lg">
                        {isNotif ? 
                        <div className="alert alert-success shadow-lg">
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" className="stroke-current flex-shrink-0 h-6 w-6" fill="none" viewBox="0 0 24 24"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" /></svg>
                                <span>{props.flash.message}</span>
                            </div>
                        </div>
                        :
                        ''}

                        <div className="text-2xl font-bold">Create Post</div>
                        <input type="text" placeholder="Judul" className="m-2 input input-bordered w-full" onChange={(title) => setTitle(title.target.value)} required/>
                        <input type="text" placeholder="Slug" className="m-2 input input-bordered w-full" onChange={(slug) => setSlug(slug.target.value)} required/>
                        <input type="text" placeholder="Deskripsi" className="m-2 input input-bordered w-full" onChange={(content) => setContent(content.target.value)} required/>
                        <select className="select select-bordered w-full max-w-xs" onChange={(category) => setCategory(category.target.value)} required>
                            <option >Select category</option>
                            <option value="1">Programming</option>
                            <option value="2">Personal</option>
                            <option value="3">Web Design</option>
                        </select>
                        <button className="m-2 btn btn-primary" onClick={() => handleSubmit()}>Post</button>
                    </div>

                    <div className="my-3 p-3 bg-white sm:rounded">
                        
                    </div>
                </div>
            </div>
        </Authenticated>
    );
}
