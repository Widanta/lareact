<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Carbon\Carbon;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Post>
 */
class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence(mt_rand(2, 8)),
            'slug' => $this->faker->slug,
            'category_id' => mt_rand(1, 3),
            // 'content' => collect($this->faker->paragraphs(mt_rand(3, 6)))
            //     ->map(fn ($p) => "<p>{$p}</p>")
            //     ->implode(''),
            'content' => $this->faker->paragraphs(3, true),
            'user_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ];
    }
}
