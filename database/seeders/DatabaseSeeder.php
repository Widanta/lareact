<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Post;
use App\Models\Category;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // password = adminadmin
        User::create([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$r3iI7ts/kGqHWbyeLjVdx.OZoFw6U5o5mz0DrTko27zDZkLvdd5X2',
            'remember_token' => null,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        Category::factory(3)->create();
        Post::factory(30)->create();
    }
}
