<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Inertia\Inertia;

class DashboardController extends Controller
{
    public function index()
    {
        $posts = Post::where('id', auth()->user()->id)->get();
        return Inertia::render('Dashboard', $posts);
    }
}
